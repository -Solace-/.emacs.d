(define-package "ctxmenu" "20140303.1342" "Provide a context menu like right-click."
  '((popup "20140205.103")
    (log4e "0.2.0")
    (yaxception "0.1"))
  :url "https://github.com/aki2o/emacs-ctxmenu" :keywords
  '("popup"))
;; Local Variables:
;; no-byte-compile: t
;; End:
