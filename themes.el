;; Gotham theme
(require 'gotham-theme)
(load-theme 'gotham t)

;; No Scroll Bar
(scroll-bar-mode -1) 

;; No Tool Bar
(tool-bar-mode -1)

;; Show Line Numbers
(global-linum-mode t)

;; no startup msg  
(setq inhibit-startup-message t)
