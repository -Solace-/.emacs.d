;; Use Company instead of auto-complete
(require 'company)
(add-hook 'after-init-hook 'global-company-mode)

;; Use popup quickhelp for displaying documentation
(company-quickhelp-mode t)
;; Make completion candidates appear instantly.
(setq company-idle-delay 0)

;; --- BACKENDS ---

;; C++, C, Objective-C
(add-hook 'c-mode-hook 'rtags-start-process-unless-running)
(add-hook 'c++-mode-hook 'rtags-start-process-unless-running)
(add-hook 'objc-mode-hook 'rtags-start-process-unless-running)

;; JavaScript
(add-to-list 'company-backends 'company-tern)


;; Python
(setq jedi:server-command '("/usr/local/bin/jediepcserver"))
(add-hook 'python-mode-hook 'jedi:setup)
(setq jedi:complete-on-dot t)
(add-to-list 'company-backends 'company-jedi)


;; --- TEMPLATES ---
(require 'yasnippet)
(yas-global-mode 1)
