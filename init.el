(package-initialize)

;; Package Management
(load "~/.emacs.d/melpa")

;; Keybindings (Vim Emulation, Window Movement, etc.)
(load "~/.emacs.d/keybindings")

;; Search Completion
(load "~/.emacs.d/searchcompletion")

;; Auto Completion (General)
(load "~/.emacs.d/autocompletion")

;; Spell Checking
(load "~/.emacs.d/spellchecking")

;; Themes and other Visual Preference
(load "~/.emacs.d/themes")

;; Support for Programming Languages
(load "~/.emacs.d/programming")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ede-project-directories (quote ("/Users/chris/Development")))
 '(package-selected-packages
   (quote
    (xelb ctxmenu helm-rtags company-rtags rtags company-jedi jedi yasnippet smartparens company-auctex company-tern xpm helm gotham-theme flycheck evil company-quickhelp auctex))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; PATH Fix
(setenv "PATH" (concat (getenv "PATH") ":/usr/local/bin"))
(setenv "PATH" (concat (getenv "PATH") ":/Library/TeX/texbin/"))
(setq exec-path (append exec-path '("/usr/local/bin")))
(setq exec-path (append exec-path '("/Library/TeX/texbin")))
(put 'upcase-region 'disabled nil)
